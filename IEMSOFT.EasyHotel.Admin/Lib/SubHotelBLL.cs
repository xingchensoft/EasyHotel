﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.EasyHotel.DAL;

namespace IEMSOFT.EasyHotel.Admin.Lib
{
    public class SubHotelBLL
    {
        public static List<SubHotelModel> GetSubHotelList(int groupHotelId)
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new SubHotelDAL(db);
                var subHotels = dal.GetList(groupHotelId);
                var model = AutoMapper.Mapper.Map<List<SubHotelModel>>(subHotels);
                return model;
            }
        }
    }
}