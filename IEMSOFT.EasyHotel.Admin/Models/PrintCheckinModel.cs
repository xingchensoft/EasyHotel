﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class PrintCheckinModel
    {
        public String BillNo { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomNo { get; set; }
        public string SubHotelName { get; set; }
        public string SubhotelAddress { get; set; }
        public string SubhotelPhone { get; set; }
        public string SubhotelFax { get; set; }
        public string BillCreatedUser { get; set; }
        public string CheckinDate { get; set; }
        public decimal SinglePrice { get; set; }
        public decimal Deposit { get; set; }
        public string TravelAgencyName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerIDNo { get; set; }
        public string PayTypeName { get; set; }
        public string PrintTime { get; set; }
        public decimal MinFeeForHourRoom { get; set; }//钟点房最低消费
        public decimal PricePerHour { get; set; }//钟点房每小时费用
        public bool IsHourRoom { get; set; } //是否是钟点房
        public string Comments { get; set; }
        public  string IsHourRoomStr
        {
            get
            {
                return IsHourRoom ? "是" : "否";
            }
        }

    }

    public class PrintCheckoutModel : PrintCheckinModel
    {
        public int StayTotalDays { get; set; }
        public int StayTotalHours { get; set; }
        public decimal RoomTotalFee { get; set; }
        public string CheckoutDate { get; set; }
        public decimal ConsumeFee { get; set; }
        public string ConsumeItem { get; set; }
        public decimal Sum { get; set; }
    }
}