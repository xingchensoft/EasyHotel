﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json.Serialization;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation.Log;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.Admin
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static ILogger _log = new SqlLogger("SiteLog");
        private static bool _shouldLogInfo = Utility.GetAppSetting<bool>(false, "ShouldLogInfo");
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutomapperConfig.Init();
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (_shouldLogInfo)
              LogHelper.RequestVariables.BeginInitVariables(true);
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (_shouldLogInfo)
            {
                LogHelper.RequestVariables.EndInitVariables();
                _log.Info("Application_EndRequest");
            }
        }

    }
}